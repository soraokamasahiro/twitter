class Tweet < ActiveRecord::Base
  validates :message, length: { maximum: 140 }  
  validates :message, presence: true
end
