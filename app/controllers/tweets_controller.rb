class TweetsController < ApplicationController

  def index
    @tweets = Tweet.all
  end
  
  def new
   @tweet = Tweet.new
  end
  
def create
    message = params[:tweet][:message]
    tweets = Tweet.new(message: message)
    tdate = params[:tweet][:tdate]
    tdate = Time.current
    tweets = Tweet.new(message: message, tdate: tdate)
    file = params[:tweet][:file].read
    tweets = Tweet.new(message: message, tdate: tdate, file: file)
    @tweet = Tweet.new(message: message, tdate: tdate, file: file)
    if @tweet.save
    flash[:notice] = "画像を保存しました。"
    redirect_to root_path 
    else
    flash[:notice] = "保存できませんでした"
    redirect_to "new"
    end
end 
  
  def destroy 
  tweets = Tweet.find(params[:id]) 
  tweets.destroy 
  redirect_to root_path
  end 

  def get_image 
   @tweet = Tweet.find(params[:id]) 
   send_data @tweet.file, :disposition => "inline", type: 'tweet/png' 
  end 

  def show
  @tweet = Tweet.find(params[:id]) 
  end  
 
  def edit 
  @tweet = Tweet.find(params[:id]) 
  end  

  def update 
  @tweet = Tweet.find(params[:id]) 
  message = params[:tweet][:message] 
  @tweet.update(message: message) 
  file = params[:tweet][:file].read
  @tweet.update(message: message, file: file)
  redirect_to root_path
  end 
end
