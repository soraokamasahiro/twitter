class CreateTweets < ActiveRecord::Migration
  def change
    create_table :tweets do |t|
      t.string :message
      t.datetime :tdate
      t.binary :file
      
      t.timestamps null: false
    end
  end
end
